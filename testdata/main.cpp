#include <algorithm>
#include <fstream>
#include <iostream>
#include <set>
#include <stdexcept>
#include <string>
#include <cstdlib>

// --size=number of int in set, required
// --out=name name of file, required

class CMDExecutor {
   public:
    CMDExecutor(int argc, const char** argv) {
        if (argc < 3) {
            throw std::invalid_argument("");
        }
        for (int i = 1; i < argc; ++i) {
            std::string str(argv[i]);
            size_t pos = str.find("=");
            std::string command_name(str.begin() + 2, str.begin() + pos);
            if (command_name == "size") {
                std::string size_number(str.begin() + pos + 1, str.end());
                this->size = std::atoi(size_number.c_str());
            }
            if (command_name == "out") {
                out_file = std::string(str.begin() + pos + 1, str.end());
            }
        }
    }

    void Execute() const {
        std::set<int> set;
        for (int i = 0; i < size; ++i) {
            set.insert(i);
        }
        std::ofstream out;
        out.open(out_file.c_str());
        std::set<int>::iterator it;
        for (it = set.begin(); it != set.end(); ++it) {
            out << *it << " ";
        }
    }

   private:
    int size;
    std::string out_file;
};

int main(int argc, const char** argv) {
    CMDExecutor executor(argc, argv);
    executor.Execute();
    return 0;
}