#ifndef PROFILE_H
#define PROFILE_H
#include <time.h>

#include <iostream>
#include <sstream>
#include <string>

class LogDuration {
   public:
    explicit LogDuration(const std::string& msg = "")
        : message(msg + ": "), start((float)clock() / CLOCKS_PER_SEC) {}

    ~LogDuration() {
        std::ostringstream os;
        os << message << ((float)clock() / CLOCKS_PER_SEC - start) * 1000
           << "ms" << std::endl;
        std::cerr << os.str();
    }

   private:
    std::string message;
    float start;
};

#endif  // !
