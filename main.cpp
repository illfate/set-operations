﻿#include <assert.h>
#include <windows.h>

#include <fstream>
#include <iostream>
#include <set>
#include <vector>

#include "profile.h"

typedef unsigned int Cell;

typedef struct HandlerInterSizeParams {
    unsigned int* setA;
    unsigned int* setB;
    unsigned int sizeA;
    unsigned int sizeB;
    Cell set1Index;
    Cell set2Index;
    unsigned int index;
    int* edgeIndexes;
} HandlerInterSizeParams, * InterSizeParams;

DWORD WINAPI multiIntersectSizeSinglePE(LPVOID lpParam) {
    HandlerInterSizeParams* params = (InterSizeParams)lpParam;
    Cell elt, egt;
    Cell i, j, k;
    Cell hi, m;
    j = params->set1Index;
    i = params->set2Index;

    i = j = k = 0;

    // полагаем, что множества не пустые

    elt = params->setB[i];
    egt = params->setA[j];

    while (true) {
        // если элемент младшего меньше элемента старшего, то
        if (elt < egt) {
            if (++i == params->sizeB) break;

            // если следующий элемент младшего также меньше элемента
            // старшего, то стараемся быстро добежать до неменьшего
            if ((elt = params->setB[i]) < egt) {
                m = 2;

                // разгоняемся
                while (true) {
                    hi = i + m;
                    m <<= 1;

                    if (hi >= params->sizeB) {
                        hi = params->sizeB;
                        if (params->setB[hi - 1] < egt) {
                            i = hi;
                            break;
                        }
                    }
                    else if (params->setB[hi] < egt) {
                        i = hi;
                        continue;
                    }
                    else
                        hi++;

                    // притормаживаем
                    while ((m = (hi - i)) > 1) {
                        m = i + (m >> 1);

                        if (params->setB[m - 1] >= egt) {
                            hi = m;
                            continue;
                        }

                        i = m;
                        if (params->setB[m] >= egt) break;
                    }
                    break;
                }

                if (i < params->sizeB)
                    elt = params->setB[i];
                else
                    break;
            }
        }

        // если элементы равны, то
        if (elt == egt) {
            // пока равны
            do {
                // добавляем в результат
                // resultSet[k++] = elt;
                k++;

                if (++i == params->sizeB) break;
                if (++j == params->sizeA) break;

                elt = params->setB[i];
                egt = params->setA[j];
            } while (elt == egt);

            if (elt == egt) break;
        }

        // если элемент старшего меньше элемента младшего, то
        if (egt < elt) {
            if (++j == params->sizeA) break;

            // если следующий элемент старшего также меньше элемента
            // младшего, то стараемся быстро добежать до неменьшего
            if (elt > (egt = params->setA[j])) {
                m = 2;

                // разгоняемся
                while (true) {
                    hi = j + m;
                    m <<= 1;

                    if (hi >= params->sizeA) {
                        hi = params->sizeA;
                        if (elt > params->setA[hi - 1]) {
                            j = hi;
                            break;
                        }
                    }
                    else if (elt > params->setA[hi]) {
                        j = hi;
                        continue;
                    }
                    else
                        hi++;

                    // притормаживаем
                    while ((m = (hi - j)) > 1) {
                        m = j + (m >> 1);

                        if (elt <= params->setA[m - 1]) {
                            hi = m;
                            continue;
                        }

                        j = m;
                        if (elt <= params->setA[m]) break;
                    }
                    break;
                }

                if (j < params->sizeA)
                    egt = params->setA[j];
                else
                    break;
            }
        }
    }

    // сохраняем размер
    params->edgeIndexes[params->index] = k;
    return 0;
}

typedef struct HandlerInterParams {
    unsigned int* setA;
    unsigned int* setB;
    Cell* resultSet;
    unsigned int sizeA;
    unsigned int sizeB;
    Cell set1Index;
    Cell set2Index;
    Cell resultStart;
    Cell resultEnd;
} HandlerInterParams, * InterParams;

DWORD WINAPI multiIntersectSinglePE(LPVOID lpParam) {
    HandlerInterParams* params = (InterParams)lpParam;

    Cell elt, egt;
    Cell i, j;
    Cell hi, m;

    Cell k = params->resultStart;

    j = params->set1Index;
    i = params->set2Index;

    // полагаем, что множества не пустые

    elt = params->setB[i];
    egt = params->setA[j];



    if (params->set1Index >= params->sizeA || params->set2Index >= params->sizeB) return 0;

    while (true) {
        // если элемент младшего меньше элемента старшего, то
        if (elt < egt) {
            if (++i == params->sizeB) break;

            // если следующий элемент младшего также меньше элемента
            // старшего, то стараемся быстро добежать до неменьшего
            if ((elt = params->setB[i]) < egt) {
                m = 2;

                // разгоняемся
                while (true) {
                    hi = i + m;
                    m <<= 1;

                    if (hi >= params->sizeB) {
                        hi = params->sizeB;
                        if (params->setB[hi - 1] < egt) {
                            i = hi;
                            break;
                        }
                    }
                    else if (params->setB[hi] < egt) {
                        i = hi;
                        continue;
                    }
                    else
                        hi++;

                    // притормаживаем
                    while ((m = (hi - i)) > 1) {
                        m = i + (m >> 1);

                        if (params->setB[m - 1] >= egt) {
                            hi = m;
                            continue;
                        }

                        i = m;
                        if (params->setB[m] >= egt) break;
                    }
                    break;
                }

                if (i < params->sizeB)
                    elt = params->setB[i];
                else
                    break;
            }
        }

        // если элементы равны, то
        if (elt == egt) {
            // пока равны
            do {
                // добавляем в результат
                if (k > params->resultEnd) {
                    break;
                }
                params->resultSet[k] = elt;
                k++;

                if (++i == params->sizeB) break;
                if (++j == params->sizeA) break;

                elt = params->setB[i];
                egt = params->setA[j];
            } while (elt == egt);

            if (elt == egt) break;
        }

        // если элемент старшего меньше элемента младшего, то
        if (egt < elt) {
            if (++j == params->sizeA) break;

            // если следующий элемент старшего также меньше элемента
            // младшего, то стараемся быстро добежать до неменьшего
            if (elt > (egt = params->setA[j])) {
                m = 2;

                // разгоняемся
                while (true) {
                    hi = j + m;
                    m <<= 1;

                    if (hi >= params->sizeA) {
                        hi = params->sizeA;
                        if (elt > params->setA[hi - 1]) {
                            j = hi;
                            break;
                        }
                    }
                    else if (elt > params->setA[hi]) {
                        j = hi;
                        continue;
                    }
                    else
                        hi++;

                    // притормаживаем
                    while ((m = (hi - j)) > 1) {
                        m = j + (m >> 1);

                        if (elt <= params->setA[m - 1]) {
                            hi = m;
                            continue;
                        }

                        j = m;
                        if (elt <= params->setA[m]) break;
                    }
                    break;
                }

                if (j < params->sizeA)
                    egt = params->setA[j];
                else
                    break;
            }
        }
    }
    return 0;
}

void ParallelIntersection(unsigned int* set1, unsigned int* set2,
                          unsigned int* result, unsigned int set1_size,
                          unsigned int set2_size, unsigned int* result_size,
                          int num_of_threads) {
    HANDLE* inter_size_handle = new HANDLE[num_of_threads];
    unsigned int size = (set1_size / num_of_threads) + 1;
    if (num_of_threads == 1) size = set1_size;
    unsigned int b = 0;
    unsigned int e = size;
    unsigned int b2 = 0;
    unsigned int e2 = 0;
    unsigned int size1 = size;
    unsigned int size2 = 0;
    unsigned int elt = set1[size];
    unsigned int egt = set1[size + 1];
    unsigned int elt2 = set2[0];
    unsigned int egt2 = set2[1];
    unsigned int m = 0;
    unsigned int prefsize2 = 0;

    int* indexs1 = new int[num_of_threads + 1];
    indexs1[0] = 0;
    indexs1[num_of_threads] = set1_size;

    int* indexs2 = new int[num_of_threads + 1];
    indexs2[0] = 0;
    indexs2[num_of_threads] = set2_size;

    int* arraySize = new int[num_of_threads + 1];
    arraySize[0] = 0;

    InterSizeParams inter_size_params[64];  // FIXME
    for (int i = 1; i <= num_of_threads; i++) {
        elt = set1[size1 - 1];
        egt = set1[size1];

        elt2 = set2[size2];
        egt2 = set2[size2 + 1];


        if (set1[size1 - 1] == set1[size1] && size1 != set1_size) {
            //Либо бинарным поиском ищем элемент что будет больше либо
            //использовать код Иваша с разгоном на поиск но переделанный
            //не знаю работает ли то что снизу но вроде логика такая
            //если что может проще скопировать код иваша и там заново изменить
            //поиск на равные потому что я мог что
            //пропустить/перепутать/потерять
            while (true) {
                // если элемент младшего меньше элемента старшего, то
                if (elt <= egt) {
                    if (++size1 == set1_size) {
                        size1;
                        break;
                    }
                    // если следующий элемент младшего также меньше элемента
                    // старшего, то стараемся быстро добежать до неменьшего
                    if ((elt = set1[size1]) < egt) {
                        m = 2;

                        // разгоняемся
                        while (true) {
                            e = size1 + e;
                            m <<= 1;

                            if (e >= set1_size) {
                                e = set1_size;
                                if (set1[e - 1] < egt) {
                                    size1 = e;
                                    size1;
                                    break;
                                }
                            }
                            else if (set1[e] < egt) {
                                size1 = e;
                                continue;
                            }
                            else
                                e++;

                            // притормаживаем
                            while ((m = (e - size1)) > 1) {
                                m = size1 + (m >> 1);

                                if (set1[m - 1] >= egt) {
                                    e = m;
                                    continue;
                                }

                                size1 = m;
                                if (set1[m] >= egt) {
                                    size1;
                                    break;
                                }
                            }
                            size1;
                            break;
                        }

                        if (size1 < set1_size)
                            elt = set1[size1];
                        else {
                            size1;
                            break;
                        }
                    }
                }
                else
                    break;
            }
        }


        egt2 = set1[size1 - 1];
        if (set2[size2] <= egt2 && size2 != set2_size && size1-1<set1_size) {
            while (true) {
                // если элемент младшего меньше элемента старшего, то
                if (elt2 <= egt2) {
                    if (++size2 == set2_size) break;

                    // если следующий элемент младшего также меньше элемента
                    // старшего, то стараемся быстро добежать до неменьшего
                    if ((elt2 = set2[size2]) < egt2) {
                        m = 2;

                        // разгоняемся
                        while (true) {
                            e2 = size2 + m;
                            m <<= 1;

                            if (e2 >= set2_size) {
                                e2 = set2_size;
                                if (set2[e2 - 1] < egt2) {
                                    size2 = e2;
                                    break;
                                }
                            }
                            else if (set2[e2] < egt2) {
                                size2 = e2;
                                continue;
                            }
                            else
                                e2++;

                            // притормаживаем
                            while ((m = (e2 - size2)) > 1) {
                                m = size2 + (m >> 1);

                                if (set2[m - 1] > egt2) {
                                    e2 = m;
                                    continue;
                                }

                                size2 = m;
                                if (set2[m] > egt2)  break;
                            }
                            break;
                        }

                        if (size2 < set2_size)
                            elt2 = set2[size2];
                        else
                            break;
                    }
                }
                else
                    break;
            }
        }

        indexs1[i] = size1;
        indexs2[i] = size2;

        inter_size_params[i - 1] = (InterSizeParams)HeapAlloc(
                GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(HandlerInterSizeParams));

        inter_size_params[i - 1]->setA = set1;
        inter_size_params[i - 1]->sizeA = size1;
        inter_size_params[i - 1]->setB = set2;
        inter_size_params[i - 1]->sizeB = size2;
        inter_size_params[i - 1]->set1Index = size1 - size;
        inter_size_params[i - 1]->set2Index = prefsize2;
        inter_size_params[i - 1]->index = i;
        inter_size_params[i - 1]->edgeIndexes = arraySize;
        inter_size_handle[i - 1] = CreateThread(
                0, 0, multiIntersectSizeSinglePE, inter_size_params[i - 1], 0,
                0);
        if (size1 + size >= set1_size-1) { size = set1_size - size1; size1 = set1_size; }
        else size1 = size1 + size;
        prefsize2 = size2;
    }
    WaitForMultipleObjects(num_of_threads, inter_size_handle, true, INFINITE);
    for (int i = 0; i < num_of_threads; i++) {
        CloseHandle(inter_size_handle[i]);
        HeapFree(GetProcessHeap(), 0, inter_size_params[i]);
    }
    HANDLE* inter_handle = new HANDLE[num_of_threads];
    InterParams inter_params[64];  // FIXME
    for (int i = 1; i <= num_of_threads; i++) {
        inter_params[i - 1] = (InterParams)HeapAlloc(
                GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(HandlerInterParams));
        inter_params[i - 1]->setA = set1;
        inter_params[i - 1]->sizeA = indexs1[i];
        inter_params[i - 1]->setB = set2;
        inter_params[i - 1]->sizeB = indexs2[i];
        inter_params[i - 1]->set1Index = indexs1[i - 1];
        inter_params[i - 1]->set2Index = indexs2[i - 1];
        inter_params[i - 1]->resultStart = arraySize[i - 1];
        inter_params[i - 1]->resultEnd = arraySize[i];
        inter_params[i - 1]->resultSet = result;
        inter_handle[i - 1] = CreateThread(0, 0, multiIntersectSinglePE,
                                           inter_params[i - 1], 0, 0);
        // multiIntersectSinglePE(set1, set2, result, indexs1[i], indexs2[i],
        //                        indexs1[i - 1], indexs2[i - 1], arraySize[i -
        //                        1], arraySize[i]);
    }
    WaitForMultipleObjects(num_of_threads, inter_handle, true, INFINITE);
    for (int i = 0; i < num_of_threads; i++) {
        CloseHandle(inter_handle[i]);
        HeapFree(GetProcessHeap(), 0, inter_params[i]);
    }
    *result_size = arraySize[num_of_threads];
    delete[] inter_size_handle;
    delete[] inter_handle;
    delete[] indexs1;
    delete[] indexs2;
    delete[] arraySize;
}

void AssertArrays(unsigned int* first, unsigned int* second, int size) {
    for (int i = 0; i < size; i++) {
        assert(first[i] == second[i]);
    }
}

void TestIntersectionSixThreads() {
    unsigned int first[] = {1, 2, 5, 6, 8, 10};

    unsigned int second[] = {3, 5, 7, 9, 10};

    unsigned int result[2];
    unsigned int expected[2] = {5, 10};
    unsigned int result_size;
    ParallelIntersection(first, second, result, 6, 5, &result_size, 6);
    assert(2 == result_size);
    AssertArrays(expected, result, 2);
}

void TestParallelSetIntersectionFourThread() {
    unsigned int first[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

    unsigned int second[] = {5, 6, 7, 8, 9, 10};

    unsigned int result[5];
    unsigned int expected[5] = {5, 6, 7, 8, 9};
    unsigned int result_size;
    ParallelIntersection(first, second, result, 9, 6, &result_size, 4);
    assert(5 == result_size);
    AssertArrays(expected, result, 5);
}

void TestParallelMultiSetIntersectionSixThread() {
    unsigned int first[] = {1, 2, 5, 5, 6, 8, 10, 10};

    unsigned int second[] = {3, 5, 5, 7, 9, 10, 10};

    unsigned int result[4];
    unsigned int expected[4] = {5, 5, 10, 10};
    unsigned int result_size;
    ParallelIntersection(first, second, result, 8, 7, &result_size, 6);
    assert(4 == result_size);
    AssertArrays(expected, result, 4);
}

void TestParallelSetIntersectionOneThread() {
    unsigned int first[] = {1, 2, 5, 6, 8, 10};

    unsigned int second[] = {3, 5, 7, 9, 10};

    unsigned int result[2];
    unsigned int expected[2] = {5, 10};
    unsigned int result_size;

    ParallelIntersection(first, second, result, 6, 5, &result_size, 1);
    assert(2 == result_size);
    AssertArrays(expected, result, 2);
}

void TestIntersection() {
    TestIntersectionSixThreads();
    TestParallelSetIntersectionFourThread();
    TestParallelSetIntersectionOneThread();
    TestParallelMultiSetIntersectionSixThread();
    std::cout << "passed\n";
}

int main() {
    TestIntersection();
    return 0;
}
